const sampleJSON = [
  {
    "name": "Accessories",
    "id": 1,
    "parent_id": 20,
  },
  {
    "name": "Watches",
    "id": 57,
    "parent_id": 1
  },
  {
    "name": "Men",
    "id": 20,
    "parent_id": null,
  }
]

const sampleSolution = [
  {
    "name": "Men",
    "id": 20,
    "parent_id": null
  },
  {
    "name": "Accessories",
    "id": 1,
    "parent_id": 20
  },
  {
    "name": "Watches",
    "id": 57,
    "parent_id": 1
  }
] //Accessories inserted before Watches since Accessories is the parent of Watches + Men before Accessories for same reason (i.e. Watches is grandchild of Men)

module.exports = function sortCategoriesForInsert (inputJson) {
  // Your code happens...
  const children = []
  const parents = []
  const childrenParentIds = []
  const grandchildrenIds =[]

 
  inputJson.map((o) => {
  	
  	if (o.parent_id != null) {
  		children.push({name: o.name, id: o.id, parent_id: o.parent_id})
  		childrenParentIds.push(o.parent_id) 
	} 
	else if (o.parent_id == null) {
	  	parents.push({name: o.name, id: o.id, parent_id: o.parent_id})
	}
  })

  const grandchildren = []

  children.map((c) => {
  	 	if (!childrenParentIds.includes(c.id)) {
  	 		grandchildren.push({name: c.name, id: c.id, parent_id: c.parent_id})
  	 		grandchildrenIds.push(c.id)
  	 	}
  })

  const uniqueCG = [...children.filter(c => !grandchildrenIds.includes(c.id)), ...grandchildren]


 
  ///   ... which calculates properJsonOutput

  const properJsonOutput = JSON.stringify(parents.concat(uniqueCG))
  return properJsonOutput
}


